import Vue from 'vue';
import Vuetify from 'vuetify';
import App from './App';
import router from './router';
import 'vuetify/dist/vuetify.min.css';

Vue.use(Vuetify);
Vue.config.productionTip = false;

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
});
