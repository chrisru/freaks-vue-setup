import Vue from 'vue';
import Router from 'vue-router';
import HelloFreak from '@/components/HelloFreak';
import ContactFreak from '@/components/ContactFreak';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloFreak',
      component: HelloFreak
    },
    {
      path: '/contact',
      name: 'ContactFreak',
      component: ContactFreak
    }
  ]
});
